import random
from estudiante import Estudiante
from asignatura import Asignatura
from diploma import Diploma
def main():
    #objetos diplomas, estudiantes y cursos
   

    programacion = Asignatura("Programacion")
    fisica = Asignatura("Fisica")
    quimica = Asignatura("Quimica")
    algebra = Asignatura("Algebra")

    opciones_nombres = ["Florencia", "Vicente", "Maria", "Julieta", "Arturo", "Adolf",
                        "Jesus", "Pia", "Paula", "Diego", "Sebastian", "Renata"]                  
    
    lista_asignatura = [programacion, fisica, quimica, algebra]
    #se crea la lista aux para sacar de ella todos los ramos e ir
    #asignandolos a alumnos, se multiplica por 3 porque son 3 diplomas por ramo
    lista_aux = lista_asignatura*3 
    lista_alumnos = []
    #se crea una cantidad aleatoria de alumnos que oscila entre la cantidad de
    #asignaturas y esta misma multiplicada por 3
    for i in range(random.randint(len(lista_asignatura), len(lista_asignatura)*3)):
        nombre = opciones_nombres.pop(random.randint(0, len(opciones_nombres)-1))
        lista_alumnos.append(Estudiante(nombre))
   
   #se agrega como minimo una asignatura a cada estudiante
    for x in lista_alumnos:
        eleccion = lista_aux.pop(random.randint(0, len(lista_aux)-1))
        x.asignatura = eleccion
    
    #aqui se toman a estudiantes de forma aleatoria para añadirles los ramos sobrantes
    #se verifica que no se repitan ramos por persona
    while True:
        if len(lista_aux) != 0:
            eleccion = random.choice(lista_aux)
            alumno = random.choice(lista_alumnos)
            if not eleccion in alumno.asignatura:
                alumno.asignatura = eleccion
                lista_aux.remove(eleccion)
        else:
            break

    diplomas = []
    for i in lista_alumnos:
        for j in i.asignatura:
            diplomas.append(Diploma(i,j))

    #se reparten diplomas de forma aleatoria a alumnos alatorios
    while len(diplomas) != 0:
        diploma = random.choice(diplomas)
        alumno = random.choice(lista_alumnos)
        alumno.diploma = diploma
        diplomas.remove(diploma)

    #se muestra toda la informacion
    print("se llamo a los siguientes alumnos: ")
    for i in lista_alumnos:
        total_asignatura = ""
        total_diplomas = ""
        for a in i.asignatura:
            total_asignatura += f"{a.nombre} "
        print(f"alumno {i.nombre} pertenece a: {total_asignatura}")
        print("poseen los diplomas: ")
        for h in i.diploma:
            total_diplomas += f"{h.estudiante.nombre} del ramo {h.asignatura.nombre}\n"
        print(total_diplomas)

    print("********************* \n")                

    #se toma el diploma de cada alumno y se busca a la que tiene
    #el nombre que sale en él
    for x in range(3):
        for alumne in lista_alumnos:
            for dip in alumne.diploma:
                for persona in lista_alumnos:
                    if dip.estudiante is persona:
                        if not dip in persona.diploma:
                            persona.diploma = dip
                            alumne.diploma.remove(dip)

    #se muetra arreglado
    print("se llamo a los siguientes alumnos: ")
    for i in lista_alumnos:
        total_asignatura = ""
        total_diplomas = ""
        for a in i.asignatura:
            total_asignatura += f"{a.nombre} "
        print(f"alumno {i.nombre} pertenece a: {total_asignatura}")
        print("poseen los diplomas: ")
        for h in i.diploma:
            total_diplomas += f"{h.estudiante.nombre} del ramo {h.asignatura.nombre}\n"
        print(total_diplomas)


if __name__ == '__main__':
    main()